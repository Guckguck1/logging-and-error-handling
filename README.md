# Express Basics: Logging and Error Handling

This project is a simple Express application that demonstrates the basics of logging and error handling.

## Features

- **Basic Routes**: The application includes the following routes:
  - (GET) /
  - (GET) /data
  - (POST) /data
  - (PUT) /data
  - (DELETE) /data

- **Logging**: The application uses the `morgan` middleware for logging details of every request, such as the HTTP method, path, and status code.

- **Error Handling**: The application includes a centralized error handling routine that catches and handles errors.

## Middleware and Error Handling

This application uses middleware for logging and has a centralized error handling routine. You can verify the functionality of these features by sending requests to the application and checking the logs. If the middleware is functioning correctly, you should see details of each request in the logs. You can also intentionally trigger errors in your route handler functions to check if the error handling routine correctly catches and handles them.

## Accessing Request Information

In the context of an Express request, you have access to various pieces of information and functions through the `req` (request) and `res` (response) objects. Here are some examples:

- `req.body`: Contains key-value pairs of data sent in the request body. When using the `body-parser` middleware, this is automatically parsed into a JavaScript object.
- `req.params`: Contains route parameters for requests with named parameters.
- `req.query`: Contains the URL query parameters.
- `req.method`: The HTTP method used for the request.
- `req.path`: The path of the request.
- `req.headers`: The HTTP headers of the request.

And many more. You can consult the Express.js documentation for a complete list of available properties and methods.

## GitLab CI/CD

This project includes a `.gitlab-ci.yml` file for continuous integration and deployment with GitLab CI/CD. The pipeline includes two stages: `install` and `test`. In the `install` stage, `npm install` is run to install all dependencies. In the `test` stage, `npm test` is run to execute the tests.

## Getting Started

### Prerequisites

- Node.js
- npm

### Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/yourusername/logging-and-error-handling.git
   ```

2. Navigate into the project directory:
    ```bash
    cd logging-and-error-handling
    ```

3. Install the dependencies:
    ```bash
    npm install
    ```

### Usage
- To start the server, run the following command:
    ```bash
    node server.js
    ```

- The server will start on port 3000. You can send requests to http://localhost:3000.

### Testing
- This project uses Jest for testing. To run the test, use the following command:
    ```bash
    npm test
    ```

## Testing with Bash

In addition to the automated tests, I'm also providing a Bash script for manual testing of the routes. The script uses `curl` to send HTTP requests to the server and print the responses.

1. To run the script, navigate to the project directory in your terminal and type:
    ```bash
    ./test.sh
    ``` 
2. Make sure that the server is running before you execute the script.

The script tests the following routes:

- (GET) /
- (GET) /data
- (POST) /data
- (PUT) /data
- (DELETE) /data

For each route, the script prints the HTTP method, the route, and the server's response. This can be useful for quickly checking if the routes are working as expected.
