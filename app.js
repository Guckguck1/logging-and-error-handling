const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('dev'));

app.get('/', (req, res) => {
    res.send('Root route');
});

app.route('/data')
    .get((req, res) => {
        res.send('GET data');
    })
    .post((req, res) => {
        res.send('POST data');
    })
    .put((req, res) => {
        res.send('PUT data');
    })
    .delete((req, res) => {
        res.send('DELETE data');
    });

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('An error occured!');
});

module.exports = app;
