const request = require('supertest');
const app = require('./app');
let server;

beforeAll(() => {
    server = require('./server');
});

afterAll(() => {
    server.close();
});

describe('Testing Express App', () => {
    test('GET / should be Root Route', async () => {
        const res = await request(app).get('/');
        expect(res.statusCode).toBe(200);
        expect(res.text).toBe('Root route');
    });

    test('GET /data should be GET data', async () => {
        const res = await request(app).get('/data');
        expect(res.statusCode).toBe(200);
        expect(res.text).toBe('GET data');
    });

    test('POST /data should be POST data', async () => {
        const res = await request(app).post('/data');
        expect(res.statusCode).toBe(200);
        expect(res.text).toBe('POST data');
    });

    test('PUT /data should be PUT data', async () => {
        const res = await request(app).put('/data');
        expect(res.statusCode).toBe(200);
        expect(res.text).toBe('PUT data');
    });

    test('DELETE /data should be DELETE data', async () => {
        const res = await request(app).delete('/data');
        expect(res.statusCode).toBe(200);
        expect(res.text).toBe('DELETE data');
    });
});
