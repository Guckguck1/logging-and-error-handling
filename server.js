const app = require('./app');

const server = app.listen(3000, () => {
    console.log('Server listens on Port 3000');
});

module.exports = server;
