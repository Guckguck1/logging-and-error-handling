#!/bin/bash

echo "GET /"
curl -i -X GET http://localhost:3000/

echo "GET /data"
curl -i -X GET http://localhost:3000/data

echo "POST /data"
curl -i -X POST http://localhost:3000/data

echo "PUT /data"
curl -i -X PUT http://localhost:3000/data

echo "DELETE /data"
curl -i -X DELETE http://localhost:3000/data
